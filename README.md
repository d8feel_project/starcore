` 喵大斯微信订阅号已开通：请在微信>通讯录>公众号>搜索“喵大斯AS3教程”以获取最及时的AS3经验知识。`
![image](https://git.oschina.net/starfire/StarSparkSkin/raw/master/_assets/wx-100x100.jpg?dir=0&filepath=_assets%2Fwx-100x100.jpg&oid=54df34f62326259a98df71256f83949d961d4a56&sha=1652abcf24111cbd8bfe9fd8b28c9c65f0f215a2)

【推荐】浙江西湖区高级人才交流，诸多猎头和企业人事汇聚，望都能找到满意的工作，招到满意的人才。
西湖区IT人才交流 QQ群：521852238
=====
-----


starcore v1.0
=====
-----

` 喵大斯核心库（含http、loader应用，及json、md5、文本模板、日期等大量工具方法）`
` 喵大斯诸多开源作品，都会基于此基础库进行开发，喜欢我作品的朋友请务必下载使用本基础库。`

##这是一个作为底层通信、常用工具方法的经典库：
  * 1、web service 通信封装
  * 2、远程加载库支持 ImageLoader、SWFLoader、XMLLoader、TextLoader、AudioLoader、BinaryLoader
  * 3、远程加载缓存管理 MediaCache，可以方便的获取和删除缓存资源，并且可对混合类型的资源进行分批管理（页游开发中常见的需要按场景对资源进行加载和清除）
  * 4、字符串模板 printf()，支持顺序替换和全局替换
  * 5、字符串处理 md5、hash、json/jsond、trim、repeat 等
  * 6、数学计算 abs、max、min、round/roundx 等高效方法替代 Math 包方法
  * 7、滤镜工具 生成和调整颜色、变换滤镜 ColorFilter
  * 8、日期时间处理 datediff、strdate、time、timens
  * 9、位图切割工具 BitmapUtil
  * 10、阻止WEB加速器的计时器 SuperTimer
  * 11、调试输出工具 Debug，单独运行无依赖

##开发计划：
  * 1、增加 Socket 通信及通信协议支持