//------------------------------------------------------------------------------
//
// interface: IModel
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-21
// copyright (c) 2013 小兵( aosnow@yeah.net )
// HTTP 服务返回响应事件
//
//------------------------------------------------------------------------------

package starfire.events
{
	import starfire.namespaces.sn_internal;
	
	import flash.events.Event;

	/**
	 * HTTP 服务返回响应事件
	 * @author AoSnow
	 */
	public class ServiceEvent extends Event
	{

		//--------------------------------------------------------------------------
		//
		//   Class constants
		//
		//--------------------------------------------------------------------------

		/** 服务请求已经成功完成  **/
		public static const COMPLETE:String = "Service_Complete";

		/** 服务请求超时  **/
		public static const REQUEST_TIMEOUT:String = "REQUEST_TIMEOUT";

		//--------------------------------------------------------------------------
		//
		//	Class properties
		//
		//--------------------------------------------------------------------------

		//----------------------------------------
		//   result
		//----------------------------------------

		private var _result:*;

		/** 服务返回的结果数据  **/
		public function get result():*
		{
			return _result;
		}

		//----------------------------------------
		//   key
		//----------------------------------------

		private var _command:String;

		/** 服务请求的命令名称  **/
		public function get command():String
		{
			return _command;
		}

		//----------------------------------------
		//   timeout
		//----------------------------------------

		private var _timeout:Boolean;

		/** 服务请求是否超时  **/
		public function get timeout():Boolean
		{
			return _timeout;
		}

		sn_internal function setTimeout( value:Boolean ):void
		{
			_timeout = value;
		}

		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------

		/**
		 *  HTTP 服务返回响应事件构造函数
		 *
		 *  @param type 事件类型
		 *  @param bubbles 指定该事件是否可以在显示列表层次结构得到冒泡处理。
		 *  @param data 指定响应返回的数据。通常这由系统自动处理
		 */
		public function ServiceEvent( type:String, key:String = null, result:* = null, timeout:Boolean = false )
		{
			super( type, false, false );

			_command = key;
			_result = result;
			_timeout = timeout;
		}

		override public function clone():Event
		{
			return new ServiceEvent( type, command, result, timeout );
		}
	}
}
