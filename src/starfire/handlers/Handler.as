//------------------------------------------------------------------------------
//
// ...
// classname: Handler
// author: 喵大斯( blog.csdn.net/aosnowasp )
// created: 2014-10-2
// copyright (c) 2013 喵大斯( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.handlers
{

	/**
	 * 事件回调处理器
	 * @author AoSnow
	 */	
	public class Handler
	{
		//--------------------------------------------------------------------------
		//
		//  Class constructor
		//
		//--------------------------------------------------------------------------

		public function Handler( method:Function = null, args:Array = null )
		{
			_method = method;
			_args = args;
		}

		//--------------------------------------------------------------------------
		//
		//	Class properties
		//
		//--------------------------------------------------------------------------

		//----------------------------------------
		//  method
		//----------------------------------------

		private var _method:Function;

		public function get method():Function
		{
			return _method;
		}

		public function set method( value:Function ):void
		{
			_method = value;
		}

		//----------------------------------------
		//  args
		//----------------------------------------

		private var _args:Array;

		public function get args():Array
		{
			return _args;
		}

		public function set args( value:Array ):void
		{
			_args = value;
		}

		//--------------------------------------------------------------------------
		//
		//	Class methods
		//
		//--------------------------------------------------------------------------

		/** 执行处理  **/
		public function execute():void
		{
			if( method is Function )
			{
				method.apply( null, args );
			}
		}

		/** 执行处理(增加数据参数) **/
		public function executeWith( data:Array ):void
		{
			if( data == null )
			{
				return execute();
			}

			if( method != null )
			{
				method.apply( null, args ? args.concat( data ) : data );
			}
		}
	}
}
