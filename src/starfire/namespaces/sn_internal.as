//------------------------------------------------------------------------------
//
// filename: sn_internal.as
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-17
// copyright (c) 2013 小兵( aosnow@yeah.net )
// 内部命名空间
//
//------------------------------------------------------------------------------

package starfire.namespaces
{
	public namespace sn_internal = "sn_internal";
}
