//------------------------------------------------------------------------------
//
//   author: 小兵（aosnow@yeah.net）
//   create: 2012-5-29 下午5:11:19
//    class: LoadState - 加载进度中的状态常量
//
//------------------------------------------------------------------------------

package starfire.rpc.loader
{
	/**
	 * 枚举加载进度中可能的状态
	 * 
	 * <p>
	 * 加载状态包含以下定义类型：
	 * <ul>
	 * <li><code>UNINITIALIZED</code> - 新创建的加载信息（LoadMessage）的初始状态</li>
	 * <li><code>LOADING</code> - 开始并正处在加载过程</li>
	 * <li><code>UNLOADING</code> - 正处在卸载过程中</li>
	 * <li><code>READY</code> - 已经加载完成，准备好被使用</li>
	 * <li><code>LOAD_ERROR</code> - 加载错误（如加载地址错误、网络原因等）</li>
	 * </ul>
	 * </p>
	 */
	public final class LoadState
	{
		/**
		 * 新创建的加载信息（LoadMessage）的初始状态
		 */
		public static const UNINITIALIZED:String	= "uninitialized";
		
		/**
		 * 开始并正处在加载过程
		 */
		public static const LOADING:String			= "loading";
		
		/**
		 * 正处在卸载过程中
		 */
		public static const UNLOADING:String		= "unloading";
		
		/**
		 * 已经加载完成，准备好被使用
		 */
		public static const READY:String			= "ready";

		/**
		 * 加载错误（如加载地址错误、网络原因等）
		 */
		public static const LOAD_ERROR:String		= "loadError";
	}
}