//------------------------------------------------------------------------------
//
// ...
// classname: Line
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-20
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.bitmaps
{
	import starfire.utils.geometry.distance;
	import starfire.utils.strings.printf;

	/**
	 * 表示一条直线的数据对象，包含 A,B 两个点坐标数据
	 * @author AoSnow
	 */
	public class SimpleLine
	{
		public var xa:Number;
		public var ya:Number;

		public var xb:Number;
		public var yb:Number;

		/** 返回线条的长度（两个点之间的距离）  **/
		public function get length():Number
		{
			if( isNaN( xa ) || isNaN( ya ) || isNaN( xb ) || isNaN( yb ))
				return 0;

			return distance( xa, ya, xb, yb );
		}

		public function toString():String
		{
			return printf( "xa:{xa},ya:{ya}  xb:{xb},yb:{yb}", xa, ya, xb, yb );
		}
	}
}
