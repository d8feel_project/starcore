//------------------------------------------------------------------------------
//
// 将参数的小数部分转换成二进制表示的字符串
// filename: decto.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-5-12
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{
	import starfire.utils.strings.str_round;

	/**
	 * 将参数的小数部分转换成二进制表示的字符串
	 * <p>整数部分将被忽略，整数转换成二进制形式请直接使用 toString(2)。</p>
	 * @param value 需要转换的参数，忽略正负符号
	 * @param radix 需要转换到的目标进制（允许的值为2、8、16，分别代表二进制、八进制、十六进制），若非法进制参数则默认使用二进制
	 * @param precision 精确到小数点后多少位（这里的位是2/8/16进制位，若结果大于此位数，则超出的第一位 0 舍 1 入[2]，3 舍 4 入[8]，7 舍 8 入[16]）
	 * @param dot 是否在小数前保留 0. 前导符，有则结果是 0.xxx 形式返回，否则只返回小数尾数部分，无 0. 前缀
	 */
	public function decto( value:Number, radix:uint = 2, precision:uint = 8, dot:Boolean = true ):String
	{
		var dec:Number = abs( value - ( value >> 0 )); // 取出小数部分
		var r:String = dot ? "0." : ""; // 返回结果的头部
		var rd:String = ""; // 小数部分（二进制）

		// 非法进制参数则默认使用二进制
		if( radix != 2 && radix != 8 && radix != 16 )
			radix = 2;

		var pos:uint = 0;
		var cur:Number = dec * radix;

		if( cur - ( cur >> 0 ) != 0 )
		{
			while( cur - ( cur >> 0 ) != 0 && pos <= precision )
			{
				rd += ( cur >> 0 ).toString( radix );
				cur = ( cur - ( cur >> 0 )) * radix;
				pos++;
			}

			// 舍入的检测
			if( cur - ( cur >> 0 ) != 0 )
			{
				rd = str_round( "0." + rd, precision, radix ).substr( 2 );
			}
		}
		else
		{
			rd += String( cur >> 0 );
		}

		return ( r + rd ).toUpperCase();
	}
}
