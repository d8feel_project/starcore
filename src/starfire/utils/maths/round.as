//------------------------------------------------------------------------------
//
// 求一个数值四舍五入后的整数部分值
// filename: round.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-1-3
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{

	/**
	 * 求一个数值四舍五入后的整数部分值
	 * @param value 需要计算的源数据
	 */
	public function round( value:Number ):Number
	{
		// 加0.5，保留整数部分，简单的方式得知整数部分是否进1		
		return value + ( value > 0 ? 0.5 : -0.5 ) >> 0;
	}
}
