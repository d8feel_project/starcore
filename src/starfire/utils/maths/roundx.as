//------------------------------------------------------------------------------
//
// 求一个数值的绝对值
// filename: roundx.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-1-3
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{

	/**
	 * 求一个数值的四舍五入值
	 * <p>这是 round 方法的加强版，可以保留任意位数的小数进行四舍五入操作，而不仅仅是整数部分</p>
	 * @param value 需要计算的源数据
	 * @param precision 默认为2，保留2位小数
	 */
	public function roundx( value:Number, precision:uint = 2 ):Number
	{
		var dig:int = Math.pow( 10, precision );

		return round( value * dig ) / dig;
	}
}
