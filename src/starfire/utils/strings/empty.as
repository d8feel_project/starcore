//------------------------------------------------------------------------------
//
// classname: hash
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	/**
	 * 检测指定的值是否为空值
	 * <p>当 value 为 null、undefined、"" 都会返回 false</p>
	 */
	public function empty( value:Object ):Boolean
	{
		return ( value == null || value == "undefined" || value == "" );
	}
}

