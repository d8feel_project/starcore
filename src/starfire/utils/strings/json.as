//------------------------------------------------------------------------------
//
// classname: json
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	import starfire.utils.jsons.JSONEncoder;

	/**
	 * 将对象编码成 JSON 格式字符串
	 *
	 * @param value 用来生成 JSON 格式字符串的对象
	 * @return 将对象转换成 JSON 格式的结果字符串
	 */
	public function json( value:Object ):String
	{
		return new JSONEncoder( value ).getString();
	}
}
