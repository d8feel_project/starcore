//------------------------------------------------------------------------------
//
// classname: openurl
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	/**
	 * 使用当前系统默认的浏览器，打开指定的 url 网址
	 * @param value url 网址
	 */
	public function openurl( url:String ):void
	{
		navigateToURL( new URLRequest( url ), "_blank" );
	}
}
