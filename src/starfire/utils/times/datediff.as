//------------------------------------------------------------------------------
//
// ...
// filename: datediff.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-4
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.times
{
	import starfire.utils.maths.abs;

	/**
	 * 比较两个秒数时间戳之间的差值
	 * @param second1 秒数
	 * @param second2 秒数，若省略第二个参数，则计算第一个参数与当前时间的差值
	 * @return 返回差值秒数
	 */
	public function datediff( second1:Number, second2:Number = -1 ):Number
	{
		var second2:Number = second2 == -1 ? ( new Date()).time / 1000 : second2;
		var sec:Number = abs( second1 - second2 ) >> 0;

		return sec;
	}
}
