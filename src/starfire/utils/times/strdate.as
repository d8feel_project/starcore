//------------------------------------------------------------------------------
//
// classname: hash
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.times
{
	import starfire.utils.maths.zeroup;
	import starfire.utils.strings.printf;

	/**
	 * 将指定参数给定的毫秒数转换为日期字符串
	 * @param format 返回日期字符串的格式，你可以自定义输出格式（y-年，m-月，d-日，h-小时，i-分钟，s-秒）
	 * @param value 自1970年1月1日以来的毫秒数（单位：毫秒）
	 * @return
	 */
	public function strdate( format:String = "{y}-{m}-{d} {h}:{i}:{s}", timestamp:Number = 0 ):String
	{
		var date:Date;

		if( timestamp > 0 )
			date = new Date( timestamp * 1000 );
		else if( timestamp == 0 )
			date = new Date();
		else
			return "";

		var y:String = String( date.getFullYear());
		var m:String = zeroup( date.getMonth() + 1 );
		var d:String = zeroup( date.getDate());
		var h:String = zeroup( date.hours );
		var i:String = zeroup( date.minutes );
		var s:String = zeroup( date.seconds );

		date = null;

		return printf( format, { y: y, m: m, d: d, h: h, i: i, s: s });
	}
}

